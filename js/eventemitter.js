//it requires only for Symbols
require('../node_modules/babel-core/browser-polyfill.js');

class EventEmitter {
    constructor() {
        this.events = {};
    }

    emit(event, ...args) {
        if (!this.events[event]) {
            return false;
        }

        for (let listener of this.events[event]) {
            try {
                listener(...args);
            } catch (e) {};
        }
    }

    addListener(event, listener) {
        if (!this.events[event]) {
            this.events[event] = [];
        }
        this.events[event].push(listener);
        return this;
    }

    on() {
        this.addListener.apply(this, arguments);
    }

    removeAllListener(event) {
        delete this.events[event];
        return this;
    }

    removeListener(event, listener) {
        if (!this.events[event]) {
            return;
        }
        var index = this.events[event].indexOf(listener);
        this.events[event].splice(index, 1);
        return this;
    }
}

export default EventEmitter;
