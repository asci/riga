import EventEmitter from "./eventemitter.js";
import DEFAULT_DATA from "data.js";

const STORE_NAME = 'tree';

class TreeStore extends EventEmitter {
    constructor() {
        super();
        this._init();
    }

    _init() {
        var data;
        var raw = localStorage.getItem(STORE_NAME);
        if (raw) {
            try {
                data = JSON.parse(raw);
            } catch (e) {
                alert('Stored tree is malformed. Restaring app with default tree');
                localStorage.setItem(STORE_NAME, JSON.stringify(DEFAULT_DATA));
                return this._init();
            }
        } else {
            data = DEFAULT_DATA;
        }
        this._data = data;
    }

    _save() {
        localStorage.setItem(STORE_NAME, JSON.stringify(this._data));
        this.emit('update');
    }

    getRef(path) {
        var keys = path.split('.');
        if (keys.length === 1) {
            return this._data;
        }
        keys.shift();
        var curr = this._data;
        for (var i = 0; i < keys.length; i++) {
            if (!curr[keys[i]]) {
                return null;
            } else {
                curr = curr[keys[i]];
            }
        }
        return curr;
    }

    removeItem(path) {
        var parent = path.split('.');
        var index = parent.pop();
        parent = parent.join('.');
        var items = this.getRef(parent);
        items.splice(index, 1);
        this._save();
    }

    addItem(path, name) {
        if (!name) {
            return;
        }
        var elem = this.getRef(path);
        if (!elem.items) {
            elem.items = [];
        }
        elem.items.push({
            name: name
        });
        this._save();
    }

    editItem(path, name) {
        if (!name) {
            return;
        }
        var elem = this.getRef(path);
        elem.name = name;
        this._save();
    }
}

export default TreeStore;
