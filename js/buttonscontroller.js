import ui from './ui.js';

class ButtonsController {
    constructor(store) {
        this._store = store;
    }

    handlePress(button) {
        switch (button.dataset.action) {
            case 'add':
                this._addItem(button);
                break;
            case 'remove':
                this._removeItem(button);
                break;
            case 'edit':
                this._editItem(button);
                break;
        };
    }

    _removeItem(button) {
        this._store.removeItem(button.dataset.path);
    }

    _addItem(button) {
        var name = ui.prompt('Name of new item');
        this._store.addItem(button.dataset.path, name);
    }

    _editItem(button) {
        var path = button.dataset.path;
        var elem = this._store.getRef(path);
        var name = ui.prompt('New name for item', elem.name);
        this._store.editItem(path, name);
    }
};

export default ButtonsController;
