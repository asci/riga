class TreeRenderer {
    _toList(array, parent) {
        let list = [];
        for (let i = array.length; i--; i < 0) {
            let elem = array[i];
            list.unshift({
                isList: true,
                value: elem,
                next: list[0] || null,
                index: i,
                parent: parent
            });
        }
        return list[0];
    }

    _getPathForListElem(elem) {
        let path = '';
        while (elem.parent) {
            path = '.items.' + elem.index + path;
            elem = elem.parent;
        }
        path = '.items.' + elem.index + path;
        path = 'root' + path;
        return path;
    }

    _renderIterativeTree(data) {
        var path = 'root';
        var result = this._renderRootElem(path, 'Iterative');
        result += '<ul>';
        var current = this._toList(data.items);

        while (current) {
            result += '<li>';
            result += this._renderElem(this._getPathForListElem(current), current.value.name);

            if (current.value.items && current.value.items.length) {
                let items = current.value.items;
                result += '<ul>';
                current = this._toList(items, current);
                continue;
            }

            if (current.next) {
                result += '</li>';
                current = current.next;
                continue;
            } else if (current.parent) {
                if (current.parent.next) {
                    current = current.parent.next;
                    result += '</li>';
                    result += '</ul>';
                    result += '</li>';
                    continue;
                } else {
                    //trying to find parent with next
                    result += '</li>';
                    while (!current.next && current.parent) {
                        current = current.parent;
                        result += '</ul>';
                        result += '</li>';
                    }
                    current = current.next;
                    continue;
                }
            }
            result += '</li>';
            current = null;
        }
        result += '</ul>';
        return result;
    }

    _renderRecursiveTree(data, path) {
        var result = '';
        if (!path) {
            path = 'root';
            result = this._renderRootElem(path, 'Recursive');
        }
        if (data.name) {
            result = this._renderElem(path, data.name);
        }
        if (data.items && data.items.length) {
            result += '<ul>';
            data.items.forEach((item, ind) => {
                result += '<li>' + this._renderRecursiveTree(item, path + '.items.' + ind) + '</li>';
            });
            result += '</ul>';
        }
        return result;
    }

    _renderRootElem(path, name) {
        return `<div class='elem'>
            <span class="root">${name}</span>
            <span class="controls">
            <button data-action='add' data-path="${path}"><i class="icon-plus"></i></button>
            </span>
        </div>`;
    }
    _renderElem(path, name) {
        return `<div class='elem'>
            <span class="name">${name}</span>
            <span class="controls">
                <button data-action='add' data-path='${path}'><i class="icon-plus"></i></button>
                <button data-action='remove' data-path='${path}'><i class="icon-minus"></i></button>
                <button data-action='edit' data-path='${path}'><i class="icon-pencil"></i></button>
            </span>
        </div>`;
    }
    render(json) {
        return `<div class="left">
            ${this._renderRecursiveTree(json)}
        </div>
        <div class="right">
            ${this._renderIterativeTree(json)}
        </div>`;
    }
}

export default TreeRenderer;
