import TreeRenderer from 'treerenderer.js';
import ButtonsController from "buttonscontroller.js";
import TreeStore from "treestore.js";

class App {
    constructor(id) {
        this._setElem(document.getElementById(id));
        this._treeRenderer = new TreeRenderer();
        this._store = new TreeStore();
        this._buttonsController = new ButtonsController(this._store);
        this._store.on('update', () => this._updateHTML());
        this._updateHTML();
    }

    _updateHTML() {
        this._elem.innerHTML = this._treeRenderer.render(this._store.getRef('root'));
    }

    _setElem(elem) {
        this._elem = elem;
        this._elem.addEventListener('click', (e) => {
            this._buttonsController.handlePress(e.target);
        });
    }
}

new App('app');
