export default {
    items: [{
        name: 'Root',
        items: [{
            name: 'super name',
            items: [{
                name: 'elem 2',
                items: [{
                    name: 'super name'
                }, {
                    name: 'super name 2'
                }, {
                    name: 'super name 3'
                }, {
                    name: 'super name 4'
                }]
            }]
        }]
    }, {
        name: 'elem one',
        items: [{
            name: 'name 2'
        }]
    }]
};
