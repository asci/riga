import TreeRenderer from '../js/treerenderer.js';

describe('TreeRenderer', function () {
    var renderer;
    var flatData = {
        items: [{
            name: 'super name'
        }, {
            name: 'super name 2'
        }, {
            name: 'super name 3'
        }, {
            name: 'super name 4'
        }]
    };

    var nestedData = {
        items: [{
            name: 'Root',
            items: [{
                name: 'super name',
                items: [{
                    name: 'elem 2',
                    items: []
                }]
            }]
        }]
    };

    var nestedWithRootsData = {
        items: [{
            name: 'Root',
            items: [{
                name: 'super name',
                items: [{
                    name: 'elem 2',
                    items: []
                }]
            }]
        }, {
            name: 'Root 2',
            items: [{
                name: 'super name',
                items: [{
                    name: 'elem 2',
                    items: []
                }]
            }]
        }]
    };

    beforeEach(function () {
        renderer = new TreeRenderer();
    });

    it('should render flat data correct', function () {
        var recursive = renderer._renderRecursiveTree(flatData);
        var iterative = renderer._renderIterativeTree(flatData);
        recursive = recursive.replace('<span class="root">Recursive</span>', '');
        iterative = iterative.replace('<span class="root">Iterative</span>', '');
        expect(recursive).toEqual(iterative);
    });
    it('should render nested data correct', function () {
        var recursive = renderer._renderRecursiveTree(nestedData);
        var iterative = renderer._renderIterativeTree(nestedData);
        recursive = recursive.replace('<span class="root">Recursive</span>', '');
        iterative = iterative.replace('<span class="root">Iterative</span>', '');
        expect(recursive).toEqual(iterative);
    });

    it('should render nested data with few roots correct', function () {
        var recursive = renderer._renderRecursiveTree(nestedWithRootsData);
        var iterative = renderer._renderIterativeTree(nestedWithRootsData);
        recursive = recursive.replace('<span class="root">Recursive</span>', '');
        iterative = iterative.replace('<span class="root">Iterative</span>', '');
        expect(recursive).toEqual(iterative);
    });
});
