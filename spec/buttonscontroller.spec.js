//sad but true — it's impossible to move declarations inside describe
import ui from "../js/ui.js";
import ButtonsController from "../js/buttonscontroller.js";

describe('ButtonsController', function () {
    var store;
    var controller;

    beforeEach(function () {
        store = jasmine.createSpyObj('store', ['removeItem', 'addItem', 'editItem', 'getRef']);
        controller = new ButtonsController(store);
    });

    it('should add item to store', function () {
        spyOn(ui, 'prompt').and.returnValue('Testname');

        controller.handlePress({
            dataset: {
                action: 'add',
                path: 'root.items.0'
            }
        });

        expect(store.addItem).toHaveBeenCalledWith('root.items.0', 'Testname');
    });

    it('should remove item from store', function () {
        controller.handlePress({
            dataset: {
                action: 'remove',
                path: 'root.items.0'
            }
        });

        expect(store.removeItem).toHaveBeenCalledWith('root.items.0');
    });

    it('should remove item from store', function () {
        spyOn(ui, 'prompt').and.returnValue('Newname');
        store.getRef.and.returnValue({});

        controller.handlePress({
            dataset: {
                action: 'edit',
                path: 'root.items.0'
            }
        });

        expect(store.getRef).toHaveBeenCalledWith('root.items.0');
        expect(store.editItem).toHaveBeenCalledWith('root.items.0', 'Newname');
    });
});
