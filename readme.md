# Tree renderer

## General questions

**Why so late?** - I tried to make this task more interest for me, so I have experiments with some technologies which I planned to try sometime. Sorry for that. Also I had some home issues.

**Why vanilla js?** — It is just more easy to implement with current requirements. I would like to use `react` or `marionette`, but I don't know how to make iterative rendering with them.

**Why no bootstrap?** — to make it more lightweight. I really don't see any benefits for so simple task.

**How much time it was take?** - a bit more than 12 hours. Almost half of this time was taken to implement iterative rendering. Yep, it is my weak point.

## Code structure
- `css` - stylesheet files for fonts and for basic design
- `dist` - bundled js file with `webpack`
- `font` - icon's font files
- `js` - source files written in es6
- `spec` - 2 files with test on `jasmine`, covered most critical code
- `index.html` - main file

## How to test
Just run `npm test`

## How to see
Just open `index.html`. It works locally, without server.

## How it looks
Two columns for two ways of render with one shared data:

![](http://content.screencast.com/users/slvrnight/folders/Jing/media/4eaa276e-2bc7-4b21-bf5c-a48d0ea1fb4f/00000213.png)
