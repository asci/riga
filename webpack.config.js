var webpack = require("webpack");

module.exports = {
    entry: "./js/app",
    resolve: {
        modulesDirectories: [
            "./js"
        ]
    },
    output: {
        publicPath: ".",
        filename: "dist/bundle.js"
    },
    module: {
        loaders: [{
            test: /\.js$/,
            loader: "babel-loader"
        }]
    }
};
